name := "ScalaWebSocket"

version := "0.1-SNAPSHOT"

scalaVersion := "2.10.4"

libraryDependencies ++= Seq("org.java-websocket" % "Java-WebSocket" % "1.3.0")