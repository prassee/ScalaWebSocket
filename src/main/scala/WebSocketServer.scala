import java.net.InetSocketAddress
import org.java_websocket.handshake.ClientHandshake
import org.java_websocket.server.WebSocketServer
import org.java_websocket.WebSocket

/**
 * A Scala wrapper around the Java-WebSocket project
 * helps to simplify web-socket programming in a functional
 * way.
 */
abstract class FrameworkServerSocket(port: Int)
  extends WebSocketServer(new InetSocketAddress(port)) {
  def onOpen(conn: WebSocket, chs: ClientHandshake): Unit = {}

  def onClose(conn: WebSocket, code: Int, reason: String,
              remote: Boolean): Unit = {}

  def onError(conn: WebSocket, ex: Exception): Unit = {}

  def onMessage(conn: WebSocket, message: String): Unit = {
    val req = toReq(message)
    handler(conn, req.params)(req.url)
  }

  def handler(conn: WebSocket, params: Map[String, String]): PartialFunction[String, Unit]

  def toReq(str: String) = WSJSONRequest("", Map("" -> ""))
}

case class WSJSONRequest(url: String, params: Map[String, String])

case class WSJSONResponse(resp: String)

/**
 *
 */
object Main extends App {
  val s = new FrameworkServerSocket(9898) {
    def handler(conn: WebSocket, params: Map[String, String]) = {
      case "/gm" => {
        conn.send("asdf")
      }
    }
  }.start()
}
